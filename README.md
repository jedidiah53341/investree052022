# investree052022

Ini merupakan repositori untuk pengerjaan task terakhir, yaitu pembuatan API dengan Spring untuk penugasan Investree Backend Developer, Rakamin Virtual Internship Program.

Berikut kumpulan screenshot tahapan-tahapan per butir soal :
![Soal 1](ss-soal-satu.jpg "Soal 1")
![Soal 2](ss-soal-dua.jpg "Soal 2")
![Soal 3](ss-soal-tiga.jpg "Soal 3")
![Soal 4](ss-soal-4.jpg "Soal 4")
![Soal 4, tes fungsi save](ss-soal-4-tes fungsi save.jpg "Soal 4, tes fungsi save")
![Soal 4, tes fungsi updateStatus](ss-soal-4-tes fungsi updateStatus.jpg "Soal 4, tes fungsi updateStatus")
![Soal 4, tes fungsi list](ss-soal-4-tes fungsi list.jpg "Soal 4, tes fungsi list")
![Soal 5](ss-soal-5.jpg "Soal 5")
![JUnit Test](output-JUnit.jpg "JUnit Test")

Gitlab : https://gitlab.com/jedidiah53341/investree052022
